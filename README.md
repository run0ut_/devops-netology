devops-netology
===============

## [DevOps и системное администрирование](./01-sysadm-homeworks/02-git-01-vcs_to_04-script-03-yaml.md)

* 8 июля — 13 октября

## [Виртуализация, базы данных и Terraform](./02-virt-homeworks/README.md)

* 28 октября — 21 января

## [CI, мониторинг и управление конфигурациями](./03-mnt-homeworks/README.md)

* 3 февраля — 20 апреля

## [Администрирование и конфигурация Kubernetes ](./04-devkub-homeworks/README.md)

* 04 мая — 30 июня

## [Kubernetes и облачные сервисы](./05-clokub-homeworks/README.md)

* 27 июля — 6 сентября

## [Дипломный блок профессии DevOps-инженер](./06-devops-diplom-yandexcloud/README.md)

* с 7 ноября, 4 недели 